<?php

use Illuminate\Http\Request;

Route::get('/clientes', 'ClienteController@index');
Route::get('/clientes/{id}', 'ClienteController@show');
Route::post('/clientes', 'ClienteController@store')->middleware('autenticar');

Route::get('/usuario', 'UsuarioController@index');