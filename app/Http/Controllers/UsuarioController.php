<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Usuario;
use App\Http\Middleware\Autenticar;

class UsuarioController extends Controller {
        /**
     * Handle an authentication attempt.
     *
     * @return Response
     */

    public function __construct() {
        $this->middleware('autenticar');
    }   

    public function index() {
        $usuario = Autenticar::usuario();
        return response(['usuario_id' => $usuario->id], 200);
    }
}
