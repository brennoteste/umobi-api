<?php

namespace App\Http\Middleware;

use Closure;
use App\Usuario;

class Autenticar {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private static $_usuario;

    public static function usuario() {
        return self::$_usuario;
    }

    public function handle($request, Closure $next) {
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));

        if (!$has_supplied_credentials) {
            return response("", 401)->header('WWW-Authenticate', 'Basic realm=Access denied');
        }

        $email = $_SERVER['PHP_AUTH_USER'];
        $senha = $_SERVER['PHP_AUTH_PW'];

        if (!isset($email, $senha)) {
            //header('HTTP/1.1 401 Authorization Required');
            //header('WWW-Authenticate: Basic realm="Access denied"');
            return response('Mensagem: falta dados de autenticação', 400)->header('Content-Type', 'text/plain');
        }

        $usuario = Usuario::where('email', $email)->first();

        if (!isset($usuario) || !password_verify($senha, $usuario->senha)) {
            return response('', 401)->header('WWW-Authenticate', 'Basic realm=Access denied');
        }
        
        self::$_usuario = $usuario;

        return $next($request);
    }
}
