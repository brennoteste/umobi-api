<?php

use Illuminate\Database\Seeder;
use App\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Cliente::create([
            'nome' => 'Hitalo',
            'email' => 'hitalo_2000@hotmail.com',
            'empresa' => 'Computers Tecnologies',
            'telefone' => '+55 (62) 98098-0909'
        ]);
        Cliente::create([
            'nome' => 'Amelia',
            'email' => 'a.melia@vicent.com',
            'empresa' => 'Vicent Moda',
            'telefone' => '+55 (14) 3400-0002'
        ]);
        Cliente::create([
            'nome' => 'Carlos',
            'email' => 'carlos.augusto@gmail.com',
            'empresa' => 'Nestle',
            'telefone' => '+55 (1) 99880-0101'
        ]);
    }
}
