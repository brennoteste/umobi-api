<?php

use Illuminate\Database\Seeder;
use App\Usuario;

class UsuariosTableSeeder extends Seeder {

    public function run() {
        Usuario::create([
            'email' => 'admin@account.com',
            'senha' => password_hash('1234', PASSWORD_BCRYPT)
        ]);
        Usuario::create([
            'email' => 'contato@account.com',
            'senha' => password_hash('01234', PASSWORD_BCRYPT)
        ]);
    }
}
